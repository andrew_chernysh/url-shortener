class UsagesController < ApplicationController
  def index
    @usages = Usage.order('created_at DESC').all
    @page_title = 'Logs'
  end
end
