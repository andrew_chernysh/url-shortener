class Url < ActiveRecord::Base
  include ActiveModel::Validations

  has_many :usages

  validates_presence_of :regular, :short
  validates_uniqueness_of :regular, :short
  validates :regular, uniqueness: true, url: true

  before_validation do
    if short.blank?
      loop do
        code = SecureRandom.hex(3)
        if self.class.find_by_short(code).blank?
          self.short = code
          break
        end
      end
    end
  end

  def to_param
    short
  end

  def to_s
    regular
  end
end
