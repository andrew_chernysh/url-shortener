class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :regular
      t.string :short
      t.integer :usages_count, null: false, default: 0

      t.timestamps null: false
    end
  end
end
