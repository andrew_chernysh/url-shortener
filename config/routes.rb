class UrlsRoutingConstraint
  def self.matches?(request)
    Url.find_by_short(request.path_parameters[:short]).present?
  end
end

Rails.application.routes.draw do
  resources :urls, param: :short
  resources :usages, only: [:index]

  root 'urls#index'

  get '*short', to: 'urls#show', constraints: UrlsRoutingConstraint, as: :short
end
