class CreateUsages < ActiveRecord::Migration
  def change
    create_table :usages do |t|
      t.references :url, index: true, foreign_key: true
      t.string :who

      t.timestamps null: false
    end
  end
end
