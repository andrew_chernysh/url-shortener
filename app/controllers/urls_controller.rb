class UrlsController < ApplicationController
  respond_to :html, :json

  before_filter :find_url, except: [:index, :new, :create]

  def index
    @urls = Url.all
    @old_urls = Url.where('created_at < ?', 14.days.ago.end_of_day)
    @page_title = 'URLs List'

    Url.where('created_at < ?', 15.days.ago.end_of_day).destroy_all
  end

  def show
    if @url.present?
      Usage.create(url: @url, who: request.remote_ip)
      redirect_to @url.regular
    else
      flash[:danger] = 'Could not find URL'
      redirect_to root_path
    end
  end

  def new
    @url = Url.new
    @page_title = 'New URL'
  end

  def create
    @url = Url.create(url_params)

    respond_with @url do |format|
      format.html do
        if @url.valid? && @url.persisted?
          flash[:success] = 'Successfully added new URL'
          redirect_to root_path
        else
          @page_title = 'New URL'
          flash.now[:danger] = 'Unable to add new URL'
          render :new
        end
      end
    end
  end

  def edit
    @page_title = 'Edit URL'
  end

  def update
    if @url.update_attributes(url_params)
      flash[:success] = 'Successfully updated URL'
      redirect_to root_path
    else
      @page_title = 'Edit URL'
      flash.now[:danger] = 'Unable to update URL'
      render :edit
    end
  end

  def destroy
    if @url.destroy
      flash[:success] = 'Successfully removed URL'
    else
      flash[:danger] = 'Unable to remove URL'
    end

    redirect_to root_path
  end

  private

  def find_url
    @url = Url.find_by_short(params[:short])
  end

  def url_params
    params.require(:url).permit(:regular, :short)
  end
end
