require 'uri'

class UrlValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    record.errors.add attribute, 'must be a valid and alive http or https URL' unless valid?(value)
  end

  private

  def valid?(url)
    uri = URI.parse(url)

    valid_protocol = [URI::HTTP, URI::HTTPS].include?(uri.class)
    valid_response =
      !(Net::HTTP.get_response(uri).kind_of?(Net::HTTPClientError) || Net::HTTP.get_response(uri).kind_of?(Net::HTTPServerError))

    valid_protocol && valid_response
  rescue URI::InvalidURIError, SocketError
    false
  end
end
